<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />
        <style type="text/css">
        	table, th, td {
			  border: 1px solid black;
			  border-collapse: collapse;
			}
        </style>
    </head>
    <body class="font-sans antialiased dark:bg-black dark:text-white/50">
	<table>
	     <thead>
	     	<tr>
	     		<th>Name/th>
	     		<th>Age</th>
	     	</tr>
	     </thead>
	     <tbody>
	     		@foreach($people as $person)
	     		<tr>
	     			<td>{{$person->name}}</td>
	     			<td>{{$person->age}}</td>
	     		</tr>
	     		@endforeach
	     </tbody>
	</table>
    </body>
</html>
